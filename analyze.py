import pandas as pd
import json
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats
import sklearn.neighbors
import sklearn.svm
from sklearn.svm import SVR
from sklearn.grid_search import GridSearchCV


total_population = {'B01003_001E': 'total_population'}

demographics = {
	'B02001_002E': 'whites',
	'B02001_003E': 'blacks',
	'B02001_005E': 'asians',
	'B01002_001E': 'median_age',
	'B03001_003E': 'hispanic',
	'B01001_002E': 'males',
	'B06009_005E': 'high_education',
	#'B24124_126E': 'religious_workers',
}

other = {
	'B08121_001E': 'median_earnings',
}

fields = {}
fields.update(total_population)
fields.update(demographics)
fields.update(other)

url = "http://api.census.gov/data/2014/acs5?get=%s&for=county:*"%(','.join(fields.keys()))
print url

data = json.load(open('data.json'))
names = data[0]
names = [fields.get(x, x) for x in names]
data = data[1:]
converted = []
for row in data:
	try:
		converted.append(map(float, row))
	except TypeError:
		continue
data = converted
data = pd.DataFrame.from_records(data, columns=names)
data['fips'] = data['state']*1000 + data['county']
del data['state']
del data['county']
data = data.set_index('fips')
for field in demographics.values():
	data[field] /= data['total_population']
#del data['total_population']
total_population = data['total_population'].copy()
data['total_population'] = np.log(data['total_population'])
data['median_earnings'] = np.log(data['median_earnings'])

for column in demographics.values():
	#data[column] = softlogit(data[column])
	data[column] = scipy.stats.boxcox(data[column].values + 0.1)[0]

for column in data.columns:
	data[column] -= data[column].mean()
	data[column] /= data[column].std()

sanders = json.load(open('sanders.json'))
sanders = pd.DataFrame({x: sanders[x] for x in ['vote_share', 'place_fips']})
sanders = sanders.set_index('place_fips')
data = data.merge(sanders, how='left', left_index=True, right_index=True)
data['vote_share'] = data['vote_share']/100.0


softer = 0.1
def softlogit(x):
	x = (x + softer)*((1 - softer)/(1 + softer))
	return np.log(x) - np.log(1 - x)
#data['vote_share'] = softlogit(data['vote_share']/100.0)
votestats = [data['vote_share'].mean(), data['vote_share'].std()]
data['vote_share'] -= votestats[0]
data['vote_share'] /= votestats[1]
orig = data.copy()
data = data
data = data.dropna()
#plt.hist(data['vote_share'].values)
#plt.show()
def denorm(x):
	v = x*votestats[1] + votestats[0]
	return v
	v = 1/(1 + np.exp(-v))
	v /= (1 - softer)/(1 + softer)
	v -= softer
	return v

#plt.hist(data['vote_share'].values)

#print dir(sklearn.neighbors)
#knn = sklearn.neighbors.KNeighborsRegressor(5, weights='distance')
knn = sklearn.svm.SVR(kernel='rbf')
#knn = GridSearchCV(SVR(kernel='rbf', gamma=0.1), cv=5,
#                   param_grid={"C": [1e0, 1e1, 1e2, 1e3],
#                               "gamma": np.logspace(-2, 2, 5)})

to_predict = 'vote_share'
y = data[to_predict].values
X = data.drop(to_predict, axis=1).values
trainingset = np.zeros(len(y), dtype=bool)
trainingset[np.random.choice(len(y), size=len(y)/2, replace=False)] = True
#print np.sum(trainingset), np.sum(~trainingset)
knn = knn.fit(X, y)
predicted = knn.predict(X)
plt.plot(denorm(y), denorm(predicted), 'k.', alpha=0.3)
plt.plot([0, 1], [0, 1], color='black')
print scipy.stats.pearsonr(denorm(y), denorm(predicted))[0]**2*100
plt.hist((denorm(y) - denorm(predicted))*100, bins=100)

future = orig[np.isnan(orig['vote_share'])]
future = future[(future.index.values/1000.0).astype(np.int) == 42]
#future = orig[(orig.index.values/1000.0).astype(np.int) == 36]
pred = knn.predict(future.drop(to_predict, axis=1).values)
future['predicted_share'] = denorm(pred)
#future = future.merge(total_population, how='left', left_index=True, right_index=True)
future['abs_total_population'] = total_population[future.index]
future['population_weight'] = future['abs_total_population']/future['abs_total_population'].sum()
plt.plot(future['vote_share'], future['predicted_share'], 'ro')


print "EST", np.mean(future['predicted_share'])
print "EST weighted", np.sum(future['predicted_share']*future['population_weight'])

plt.show()

#plt.hist(future['population_weight'].values)
#plt.show()
